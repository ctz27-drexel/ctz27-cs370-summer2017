#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <iterator>
#include <cstdlib>

typedef unsigned char BYTE;

using namespace std;

// convert a vector of 1, 2, or 4 bytes to an integer
int from_bytes(vector<BYTE> byte_vector) {
	if(byte_vector.size() == 1)
		return (int) byte_vector[0];
	else if(byte_vector.size() == 2) {
		int val = (byte_vector[0] << 8) | byte_vector[1];
		return val;
	}
	else {
		int val = (byte_vector[0] << 8) | byte_vector[1];
		val = val << 8 | byte_vector[2];
		val = val << 8 | byte_vector[3];
		return val;
	}
}

string bytesToString(vector<BYTE> byte_vector) {
	string str;

	for(int i=0; i<byte_vector.size(); i++) {
		int intVal = byte_vector[i];
		//printf("%s ", (char)intVal);
		str += (char)intVal;
	}
	
	return str;
}

class Disk {
	public:
		vector<BYTE> file_hex;
		
		string volumeLabel;
		vector<BYTE> serialNum;
		int bytesPerBlock;
		int blocksPerAllocationUnit;
		int numReservedBlocks;
		int numFATs;
		int numRootDirEntries;
		int sizeOfFAT;
		
		int rootDirLocation;
        int fileDataLocation;
	
		Disk(const char* filename) {
			std::ifstream file(filename, ios::binary);
			
			file.unsetf(std::ios::skipws);
			
			streampos fileSize;
			
			file.seekg(0, ios::end);
			fileSize = file.tellg();
			file.seekg(0, ios::beg);
			
			file_hex.reserve(fileSize);
			file_hex.insert(file_hex.begin(), istream_iterator<BYTE>(file), istream_iterator<BYTE>());
			
			bytesPerBlock = from_bytes(readBytes(11, 2, "little"));
			blocksPerAllocationUnit = from_bytes(readBytes(13, 1, "little"));
			numReservedBlocks = from_bytes(readBytes(14, 2, "little"));
			numFATs = from_bytes(readBytes(16, 1, "little"));
			numRootDirEntries = from_bytes(readBytes(17, 2, "little"));
			sizeOfFAT = from_bytes(readBytes(22, 2, "little"));
			rootDirLocation = (sizeOfFAT * numFATs + 1) * bytesPerBlock;
			volumeLabel = bytesToString(readBytes(43, 11, "big"));
			serialNum = readBytes(39, 4, "little");
            fileDataLocation = rootDirLocation + (numRootDirEntries*32);
		}
	
		vector<BYTE> readBytes(int offset, int size, string endian) {
			vector<BYTE> subVector;
			
			if(endian == "big") {
				for(int i=offset; i<offset+size; i++) {
					subVector.push_back(file_hex[i]);
				}
			}
			else {
				for(int i=offset+size-1; i>=offset; i--) {
					subVector.push_back(file_hex[i]);
				}
			}
			
			return subVector;
		}
};

class FAT {
public:
    int fatLocation;

    Disk* disk;

    FAT(Disk* d) {
        disk = d;
    }

    int getNextClusterLocation(int clusterNum) {
        int part1or2 = clusterNum % 2;
        int entryLoc = (((clusterNum - part1or2) / 2) * 3);
        vector<BYTE> entry = disk->readBytes((entryLoc + disk->bytesPerBlock), 3, "big");

        int UV=0, WX=0, YZ=0;
        UV = entry[0];
        WX = entry[1];
        YZ = entry[2];

        int temp = 15;
        int d = (WX & temp);

        if(part1or2 == 0) {
            return ((WX & 15) << 8) | UV;
        }
        else {
            return (YZ << 4) | ((WX & 240) >> 4);
        }
    }
};

class DirEntry {
	public:
		string filename;
		string fileExt;
		int timeBuf;
		int hour;
		int minute;
		int dateBuf;
		int year;
		int month;
		int day;
		int startCluster;
		int fileSize;
		int init_offset;
		
		Disk* disk;
        vector<BYTE> fileData;
		
		DirEntry(int offset, Disk* d) {
			disk = d;
			init_offset = offset;
			filename = bytesToString(readBytes(0, 8, "big"));
            filename.erase(filename.find_last_not_of(" ")+1);  // remove spaces from filename
			fileExt = bytesToString(readBytes(8, 3, "big"));
            fileExt.erase(fileExt.find_last_not_of(" ")+1);  // remove spaces from fileExt
			timeBuf = from_bytes(readBytes(22, 2, "little"));
			hour = (timeBuf >> 11);
			minute = (timeBuf >> 5) & 63;
			dateBuf = from_bytes(readBytes(24, 2, "little"));
			year = 1980 + (dateBuf >> 9);
			month = (dateBuf >> 5) & 15;
			day = dateBuf & 31;
			startCluster = from_bytes(readBytes(26, 2, "little"));
			fileSize = from_bytes(readBytes(28, 4, "little"));
            getFileData();  // gets file data and stores it in fileData
            saveFile();
		}
		
		vector<BYTE> readBytes(int offset, int size, string endian) {
			return disk->readBytes((init_offset+offset), size, endian);
		}

        void getFileData() {
            FAT* fat = new FAT(disk);
            int clusterOffset = disk->fileDataLocation + ((startCluster - 2) * disk->bytesPerBlock);
            int blockLoc = clusterOffset / disk->bytesPerBlock;
            int bytesToRead = fileSize;

            while (bytesToRead > 0) {
                if(blockLoc <= 4079 && blockLoc >= 2) {
                    int readNumBytes;
                    if (bytesToRead > disk->bytesPerBlock)
                        readNumBytes = disk->bytesPerBlock;
                    else
                        readNumBytes = bytesToRead;

                    vector <BYTE> tempData = disk->readBytes(clusterOffset, readNumBytes, "big");
                    fileData.insert(fileData.end(), tempData.begin(), tempData.end());

                    blockLoc = fat->getNextClusterLocation(blockLoc);
                    clusterOffset = blockLoc * disk->bytesPerBlock;
                    bytesToRead -= readNumBytes;
                }
                else
                    break;
            }
        }

        void saveFile() {
            string saveFilename = "./MSDOS/" + filename + "." + fileExt;
            if(fileExt != "EX_" && fileExt != "TXT" && fileExt != "DO_" && fileExt != "INI" && fileExt != "CO_" && fileExt != "386" && fileExt != "DL_" && fileExt != "flp")
                saveFilename = "./MSDOS/" + filename;

            ofstream file(saveFilename.c_str(), ios::out | ios::binary);
            file.write((const char*)&fileData[0], fileData.size());

            file.close();
        }
};

int main(int argc, char *argv[]) {
	const char* filename;
	int numFiles = 0;
	
	if(argc != 2) {
		cout << "Usage: msdosextr <FILENAME>" << endl;
		return 0;
	}
	else {
		filename = argv[1];
	}
	
	Disk *disk = new Disk(filename);
	
	for(int i=disk->rootDirLocation; i<(disk->numRootDirEntries*32+disk->rootDirLocation); i+=32) {
		// check if the first byte is 0xe5 or 0x00
		int firstByte = from_bytes(disk->readBytes(i, 1, "big"));
		if(firstByte != 229 && firstByte != 0) {
			numFiles++;
		}
	}
	
	printf("Extracting: %d files\n", numFiles);

    // directory to store the files in
    int dir_err = system("mkdir -p ./MSDOS");
    if(dir_err == -1) {
        printf("Error creating directory for files\n");
        exit(1);
    }

	for(int i=disk->rootDirLocation; i<(disk->numRootDirEntries*32+disk->rootDirLocation); i+=32) {
		// check if the first byte is 0xe5 or 0x00
		int firstByte = from_bytes(disk->readBytes(i, 1, "big"));
		if(firstByte != 229 && firstByte != 0) {
			DirEntry *file = new DirEntry(i, disk);
			string filename = file->filename.c_str();
			string fileExt = file->fileExt.c_str();

            if(fileExt != "EX_" && fileExt != "TXT" && fileExt != "DO_" && fileExt != "INI" && fileExt != "CO_" && fileExt != "386" && fileExt != "DL_" && fileExt != "flp")
                printf("%s, ", file->filename.c_str());
            else
                printf("%s.%s, ", file->filename.c_str(), file->fileExt.c_str());
		}
	}
    cout << endl;
	
	return 0;
}
