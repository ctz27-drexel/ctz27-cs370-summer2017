#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <iterator>
#include <cstdlib>

typedef unsigned char BYTE;

using namespace std;

// convert a vector of 1, 2, or 4 bytes to an integer
int from_bytes(vector<BYTE> byte_vector) {
	if(byte_vector.size() == 1)
		return (int) byte_vector[0];
	else if(byte_vector.size() == 2) {
		int val = (byte_vector[0] << 8) | byte_vector[1];
		return val;
	}
	else {
		int val = (byte_vector[0] << 8) | byte_vector[1];
		val = val << 8 | byte_vector[2];
		val = val << 8 | byte_vector[3];
		return val;
	}
}

string bytesToString(vector<BYTE> byte_vector) {
	string str;

	for(int i=0; i<byte_vector.size(); i++) {
		int intVal = byte_vector[i];
		//printf("%s ", (char)intVal);
		str += (char)intVal;
	}
	
	return str;
}

class Disk {
	public:
		vector<BYTE> file_hex;
		
		string volumeLabel;
		vector<BYTE> serialNum;
		int bytesPerBlock;
		int blocksPerAllocationUnit;
		int numReservedBlocks;
		int numFATs;
		int numRootDirEntries;
		int sizeOfFAT;
		
		int rootDirLocation;
	
		Disk(const char* filename) {
			std::ifstream file(filename, ios::binary);
			
			file.unsetf(std::ios::skipws);
			
			streampos fileSize;
			
			file.seekg(0, ios::end);
			fileSize = file.tellg();
			file.seekg(0, ios::beg);
			
			file_hex.reserve(fileSize);
			file_hex.insert(file_hex.begin(), istream_iterator<BYTE>(file), istream_iterator<BYTE>());
			
			bytesPerBlock = from_bytes(readBytes(11, 2, "little"));
			blocksPerAllocationUnit = from_bytes(readBytes(13, 1, "little"));
			numReservedBlocks = from_bytes(readBytes(14, 2, "little"));
			numFATs = from_bytes(readBytes(16, 1, "little"));
			numRootDirEntries = from_bytes(readBytes(17, 2, "little"));
			sizeOfFAT = from_bytes(readBytes(22, 2, "little"));
			rootDirLocation = (sizeOfFAT * numFATs + 1) * bytesPerBlock;
			volumeLabel = bytesToString(readBytes(43, 11, "big"));
			serialNum = readBytes(39, 4, "little");
		}
	
		vector<BYTE> readBytes(int offset, int size, string endian) {
			vector<BYTE> subVector;
			
			if(endian == "big") {
				for(int i=offset; i<offset+size; i++) {
					subVector.push_back(file_hex[i]);
				}
			}
			else {
				for(int i=offset+size-1; i>=offset; i--) {
					subVector.push_back(file_hex[i]);
				}
			}
			
			return subVector;
		}
};

class DirEntry {
	public:
		string filename;
		string fileExt;
		int timeBuf;
		int hour;
		int minute;
		int dateBuf;
		int year;
		int month;
		int day;
		int startCluster;
		int fileSize;
		int init_offset;
		
		Disk* disk;
		
		DirEntry(int offset, Disk* d) {
			disk = d;
			init_offset = offset;
			filename = bytesToString(readBytes(0, 8, "big"));
			fileExt = bytesToString(readBytes(8, 3, "big"));
			timeBuf = from_bytes(readBytes(22, 2, "little"));
			hour = (timeBuf >> 11);
			minute = (timeBuf >> 5) & 63;
			dateBuf = from_bytes(readBytes(24, 2, "little"));
			year = 1980 + (dateBuf >> 9);
			month = (dateBuf >> 5) & 15;
			day = dateBuf & 31;
			startCluster = from_bytes(readBytes(26, 2, "little"));
			fileSize = from_bytes(readBytes(28, 4, "little"));
		}
		
		vector<BYTE> readBytes(int offset, int size, string endian) {
			return disk->readBytes((init_offset+offset), size, endian);
		}
};

int main(int argc, char *argv[]) {
	const char* filename;
	int totalBytes = 0;
	int numFiles = 0;
	
	if(argc != 2) {
		cout << "Usage: msdosdir <FILENAME>" << endl;
		return 0;
	}
	else {
		filename = argv[1];
	}
	
	Disk *disk = new Disk(filename);

	printf("Volume name is %s\n", disk->volumeLabel.c_str());
	printf("Volume Serial Number is %x%x-%x%x\n", disk->serialNum[0], disk->serialNum[1], disk->serialNum[2], disk->serialNum[3]);
	cout << endl;
	
	//printf("%s\n", filename);
	
	for(int i=disk->rootDirLocation; i<(disk->numRootDirEntries*32+disk->rootDirLocation); i+=32) {
		// check if the first byte is 0xe5 or 0x00
		int firstByte = from_bytes(disk->readBytes(i, 1, "big"));
		if(firstByte != 229 && firstByte != 0) {
			DirEntry *file = new DirEntry(i, disk);
			numFiles++;
			totalBytes += file->fileSize;
				
			printf("%8s ", file->filename.c_str());
			printf("%3s     ", file->fileExt.c_str());
			printf("%'5d ", file->fileSize);
			printf("%02d-%02d-%d  ", file->month, file->day, file->year);
			printf("%d:%02d", file->hour, file->minute);
			cout << endl;
		}
	}
	printf("     %d file(s)     %d bytes\n", numFiles, totalBytes);
	
	return 0;
}
