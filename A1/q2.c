#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char **argv) {
        pid_t parent_pid = getpid();
        pid_t child_pid;
        pid_t grandchild_pid;
        pid_t pid;

        pid = fork();

        if(pid >= 0) {  // fork was successful
                if(pid == 0) {  // child process
                        child_pid = getpid();
                        pid = fork();

                        if(pid >= 0) {  // fork success
                                if(pid == 0) {  // granchild process
                                        grandchild_pid = getpid();

                                        printf("My process id is %d, my parent's id is %d, and my Grandparent's id is %d\n", grandchild_pid, child_pid, parent_pid);
                                }
                        }
                }
        }

        return 0;
}
