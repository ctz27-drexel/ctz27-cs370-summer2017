#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

bool caught_signal = false;

void sig_handler(int sig_num) {
        if(sig_num == SIGUSR1) {
                printf("Received signal from first child\n");
                caught_signal = true;
        }
}

int main() {
        signal(SIGUSR1, sig_handler);  // signal handler

        pid_t first_child_pid, second_child_pid, third_child_pid;
        pid_t parent_pid = getpid();

        first_child_pid = fork();

        if(first_child_pid == 0) {
                kill(parent_pid, SIGUSR1);  // send signal to parent
                exit(0);
        }

        second_child_pid = fork();

        if(second_child_pid == 0) {
                sleep(10);  // wait 10 seconds before exiting
                exit(0);
        }

        third_child_pid = fork();

        if(third_child_pid == 0) {
                // infinite while loop until parent kills this process
                while(1) {
                        sleep(1);
                        printf("Third Child\n");
                }
        }

        wait(0);
        wait(0);
        while(!caught_signal);  // wait for the first child's signal to be caught
        kill(third_child_pid, SIGKILL);  // kill the third child

        exit(0);
}